import 'package:flutter/material.dart';
import 'package:flutter_scale/screens/addproduct/add_product_screen.dart';
import 'package:flutter_scale/screens/dashboard/dashboard_screen.dart';
import 'package:flutter_scale/screens/drawermenu/about_screen.dart';
import 'package:flutter_scale/screens/drawermenu/contact_screen.dart';
import 'package:flutter_scale/screens/drawermenu/info_screen.dart';
import 'package:flutter_scale/screens/login/login_screen.dart';
import 'package:flutter_scale/screens/login/register_screen.dart';
import 'package:flutter_scale/screens/newsdetail/newsdetail_screen.dart';
import 'package:flutter_scale/screens/scanresult/scan_result_scree.dart';
import 'package:flutter_scale/screens/updateproduct/update_product_screen.dart';
import 'package:flutter_scale/screens/welcome/welcome_screen.dart';

// สร้างตัวแปรแบบ Map เก็บ Route ไว้
Map<String, WidgetBuilder> routes = {
  '/welcome': (BuildContext context) => const WelcomeScreen(),
  '/login': (BuildContext context) => const LoginScreen(),
  '/register': (BuildContext context) => const RegisterScreen(),
  '/about': (BuildContext context) => const AboutScreen(),
  '/info': (BuildContext context) => const InfoScreen(),
  '/contact': (BuildContext context) => const ContactScreen(),
  '/dashboard': (BuildContext context) => const DashboardScreen(),
  '/newsdetail': (BuildContext context) => const NewsdetailScreen(),
  '/addproduct': (BuildContext context) => const AddProductScreen(),
  '/updateproduct': (BuildContext context) => const UpdateProductScreen(),
  '/scanresult': (BuildContext context) => ScanResultScreen()
};
