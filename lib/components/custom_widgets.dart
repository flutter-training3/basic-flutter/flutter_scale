import 'package:flutter/material.dart';
import 'package:flutter_scale/themes/colors.dart';

Widget submitButton(
    String btnText, Color? btnColor, Color? textColor, Function() onPress) {
  return SizedBox(
    width: double.infinity,
    height: 40,
    child: ElevatedButton(
        onPressed: onPress,
        style: ElevatedButton.styleFrom(
          backgroundColor: btnColor,
        ),
        child: Text(
          btnText,
          style: TextStyle(color: textColor, fontSize: 18),
        )),
  );
}

Widget inputFieldWidget(BuildContext context, Icon icon, String hintText,
    String errorText, Function onValidate, void Function(String) onSave,
    {String initialValue = '',
    bool autoFocus = false,
    bool obsecureText = false,
    int maxLenght = 30,
    TextInputType keyboardType = TextInputType.text,
    var maxLine = 1}) {
  return TextFormField(
    autofocus: autoFocus,
    initialValue: initialValue,
    obscureText: obsecureText,
    validator: (value) {
      return onValidate(value, errorText);
    },
    onSaved: (value) {
      return onSave(value.toString().trim());
    },
    maxLength: maxLenght,
    maxLines: maxLine,
    keyboardType: keyboardType,
    decoration: InputDecoration(
        hintText: hintText,
        hintStyle: const TextStyle(color: secondaryText, fontSize: 14),
        contentPadding:
            const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        errorStyle: TextStyle(color: Colors.red[600]),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: const BorderSide(color: primary, width: 1)),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: const BorderSide(color: primaryLight, width: 2)),
        prefixIcon: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child:
              IconTheme(data: const IconThemeData(color: primary), child: icon),
        )),
  );
}
