import 'dart:convert';

import 'package:flutter_scale/models/news_detail_model.dart';
import 'package:flutter_scale/models/news_model.dart';
import 'package:flutter_scale/models/product_model.dart';
import 'package:flutter_scale/utils/constants.dart';
import 'package:http/http.dart' as http;

class CallApi {
  // กำหนด Header สำหรับเรียก api
  _setHeader() =>
      {'Content-Type': 'application/json', 'Accept': 'application/json'};

  // สร้างฟังก์ชั่นการ Login
  loginApi(data) async {
    return await http.post(Uri.parse('${baseURLAPI}login'),
        body: json.encode(data), headers: _setHeader());
  }

  // สร้างฟังก์ชั่นการ Register
  registerApi(data) async {
    return await http.post(Uri.parse('${baseURLAPI}register'),
        body: json.encode(data), headers: _setHeader());
  }

  // สร้างฟังก์ชั่นในการอ่านข่าวทั้งหมด
  Future<List<NewsModel>?> getAllNews() async {
    final response =
        await http.get(Uri.parse('${baseURLAPI}news'), headers: _setHeader());
    if (response.body != null) {
      return newsModelFromJson(response.body);
    } else {
      return null;
    }
  }

  // สร้างฟังก์ชั่นในการอ่านข่าวล่าสุด
  Future<List<NewsModel>?> getLastNews() async {
    final response = await http.get(Uri.parse('${baseURLAPI}lastnews'),
        headers: _setHeader());
    if (response.body != null) {
      return newsModelFromJson(response.body);
    } else {
      return null;
    }
  }

  // สร้างฟังก์ชั่นในการอ่านรายละเอียดข่าว
  Future<NewsDetailModel?> getNewsDetail(id) async {
    final response = await http.get(Uri.parse('${baseURLAPI}news/$id'),
        headers: _setHeader());
    if (response.body != null) {
      return newsDetailModelFromJson(response.body);
    } else {
      return null;
    }
  }

  // ----------------------------
  // ส่วนของการ CRUD Product
  // ----------------------------
  // * Get All Products
  Future<List<ProductModel>?> getProducts() async {
    final response = await http.get(Uri.parse('${baseURLAPI}products'),
        headers: _setHeader());

    if (response.body != null) {
      return productsModelFromJson(response.body);
    } else {
      return null;
    }
  }

  // * Add New Product
  Future<bool> createProduct(ProductModel data) async {
    final response = await http.post(Uri.parse('${baseURLAPI}products'),
        body: productModelToJson(data), headers: _setHeader());
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  // * Edit Product
  Future<bool> updateProduct(ProductModel data) async {
    final response = await http.put(
        Uri.parse('${baseURLAPI}products/${data.id}'),
        body: productModelToJson(data),
        headers: _setHeader());
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  // * Delete Product
  Future<bool> deleteProduct(String id) async {
    final response = await http.delete(Uri.parse('${baseURLAPI}products/$id'),
        headers: _setHeader());
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
