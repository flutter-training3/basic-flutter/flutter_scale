import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

class Utility {
  static Utility? _utility;
  static Utility? getInstance() {
    _utility ??= Utility();
    return _utility;
  }

  // * สร้างฟังก์ชั่นสำหรับแสดง alert
  showAlertDialog(BuildContext context,
      {required String altTitle,
      required String altContent,
      required String altBtnText}) {
    AlertDialog alert = AlertDialog(
      title: Text(altTitle),
      content: Text(altContent),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context), child: Text(altBtnText))
      ],
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => alert,
    );
  }

  // * สร้างฟังก์ชั่นสำหรับการเชื่อมต่อ Network
  Future<String> checkNetwork() async {
    var checkNetwork = await Connectivity().checkConnectivity();
    switch (checkNetwork) {
      case ConnectivityResult.none:
        return '';
      case ConnectivityResult.bluetooth:
        return 'bluetooth';
      case ConnectivityResult.ethernet:
        return 'ethernet';
      case ConnectivityResult.mobile:
        return 'mobile';
      case ConnectivityResult.wifi:
        return 'wifi';
      default:
        return '';
    }
  }
}
