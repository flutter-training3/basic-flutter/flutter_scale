// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_scale/components/custom_widgets.dart';
import 'package:flutter_scale/models/product_model.dart';
import 'package:flutter_scale/services/rest_api.dart';
import 'package:flutter_scale/themes/colors.dart';
import 'package:flutter_scale/utils/utility.dart';

class UpdateProductScreen extends StatefulWidget {
  const UpdateProductScreen({Key? key}) : super(key: key);

  @override
  State<UpdateProductScreen> createState() => _UpdateProductScreenState();
}

class _UpdateProductScreenState extends State<UpdateProductScreen> {
  final formKey = GlobalKey<FormState>();
  String? _productName,
      _productDetail,
      _productBarcode,
      _productPrice,
      _productQty,
      _productImage,
      _id;

  // * สร้างฟังก์ชั่น Validate Input
  _validateFunction(String validate, String errorText) {
    if (validate.isEmpty) {
      return errorText;
    } else {
      return null;
    }
  }

  void _submitProduct() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      var utility = Utility.getInstance();
      String checkNetwork = await utility!.checkNetwork();
      if (checkNetwork == '') {
        utility.showAlertDialog(context,
            altTitle: 'มีข้อผิดพลาดเกิดขึ้น !',
            altContent: 'อุปกรณ์ของคุณไม่ได้เชื่อมต่ออินเตอร์เน็ต!',
            altBtnText: 'ตกลง');
      } else {
        ProductModel productModel = ProductModel(
            id: _id,
            productDetail: _productDetail!,
            productName: _productName!,
            productBarcode: _productBarcode!,
            productQty: _productQty!,
            productPrice: _productPrice!,
            productImage: _productImage!);
        var response = await CallApi().updateProduct(productModel);
        if (response) {
          Navigator.pop(context);
        } else {
          utility.showAlertDialog(context,
              altTitle: 'มีข้อผิดพลาดเกิดขึ้น !',
              altContent:
                  'ไม่สามารถบันทึกข้อมูลสินค้า กรุณาตรวจสอบข้อมูลใหม่อีกครั้ง!',
              altBtnText: 'ตกลง');
        }
      }
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    _id = args.id;
    return Scaffold(
      appBar: AppBar(
        title: const Text('แก้ไขสินค้า'),
      ),
      body: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 15),
            child: ListView(
              children: [
                inputFieldWidget(
                    context,
                    const Icon(Icons.production_quantity_limits_outlined),
                    'ชื่อสินค้า',
                    'กรุณากรอกข้อมูลชื่อสินค้า',
                    _validateFunction, (valueSave) {
                  _productName = valueSave;
                }, maxLenght: 100, initialValue: args.productName),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(
                    context,
                    const Icon(Icons.list_alt_outlined),
                    'รายละเอียดสินค้า',
                    'กรุณากรอกข้อมูลรายละเอียดสินค้า',
                    _validateFunction, (valueSave) {
                  _productDetail = valueSave;
                },
                    maxLenght: 500,
                    keyboardType: TextInputType.multiline,
                    maxLine: 5,
                    initialValue: args.productDetail),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(context, const Icon(Icons.code), 'บาร์โค๊ด',
                    'กรุณากรอกข้อมูลบาร์โค๊ด', _validateFunction, (valueSave) {
                  _productBarcode = valueSave;
                },
                    maxLenght: 50,
                    keyboardType: TextInputType.number,
                    initialValue: args.productBarcode),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(
                    context,
                    const Icon(Icons.price_change),
                    'ราคาสินค้า',
                    'กรุณากรอกข้อมูลราคาสินค้า',
                    _validateFunction, (valueSave) {
                  _productPrice = valueSave;
                },
                    maxLenght: 10,
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    initialValue: args.productPrice),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(
                    context,
                    const Icon(Icons.store),
                    'จำนวนสินค้า',
                    'กรุณากรอกข้อมูลจำนวนสินค้า',
                    _validateFunction, (valueSave) {
                  _productQty = valueSave;
                },
                    maxLenght: 10,
                    keyboardType: TextInputType.number,
                    initialValue: args.productQty),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(
                    context,
                    const Icon(Icons.image),
                    'รูปภาพสินค้า',
                    'กรุณากรอกข้อมูลรูปภาพสินค้า',
                    _validateFunction, (valueSave) {
                  _productImage = valueSave;
                },
                    maxLenght: 100,
                    keyboardType: TextInputType.url,
                    initialValue: args.productImage),
                const SizedBox(
                  height: 15,
                ),
                submitButton(
                    'บันทึกข้อมูล', primaryDark, primaryLight, _submitProduct),
              ],
            ),
          )),
    );
  }
}

class ScreenArguments {
  final String id,
      productName,
      productDetail,
      productBarcode,
      productPrice,
      productQty,
      productImage;
  ScreenArguments(
      this.id,
      this.productName,
      this.productDetail,
      this.productBarcode,
      this.productPrice,
      this.productQty,
      this.productImage);
}
