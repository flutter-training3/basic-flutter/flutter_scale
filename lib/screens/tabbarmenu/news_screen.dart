import 'package:flutter/material.dart';
import 'package:flutter_scale/models/news_model.dart';
import 'package:flutter_scale/screens/tabbarmenu/widgets/news_item_horizontal.dart';
import 'package:flutter_scale/screens/tabbarmenu/widgets/news_items_vertical.dart';
import 'package:flutter_scale/services/rest_api.dart';
import 'package:flutter_scale/themes/colors.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({Key? key}) : super(key: key);

  @override
  State<NewsScreen> createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        setState(() {});
      },
      child: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Text(
              'ข่าวประกาศล่าสุด',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 210,
            child: FutureBuilder(
              future: CallApi().getLastNews(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return const Center(
                    child: Text('เกิดข้อผิดพลาดในการแสดงข้อมูล'),
                  );
                } else if (snapshot.connectionState == ConnectionState.done) {
                  // * แสดงข้อมูล
                  List<NewsModel> news = snapshot.data!;
                  return lastNewsItemList(news);
                } else {
                  // * แสดง Loading กรณียังโหลดข้อมูลไม่สำเร็จ
                  return const Center(
                    child: CircularProgressIndicator(color: primaryLight),
                  );
                }
              },
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Text(
              'ข่าวทั้งหมด',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
              height: (MediaQuery.of(context).size.height * 0.4),
              child: FutureBuilder(
                future: CallApi().getAllNews(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return const Center(
                      child: Text('เกิดข้อผิดพลาดในการแสดงข้อมูล'),
                    );
                  } else if (snapshot.connectionState == ConnectionState.done) {
                    // * แสดงข้อมูล
                    List<NewsModel> news = snapshot.data!;
                    return newsItemList(news);
                  } else {
                    // * แสดง Loading กรณียังโหลดข้อมูลไม่สำเร็จ
                    return const Center(
                      child: CircularProgressIndicator(color: primaryLight),
                    );
                  }
                },
              ))
        ],
      )),
    );
  }
}
