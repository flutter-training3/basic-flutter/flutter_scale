import 'package:flutter/material.dart';
import 'package:flutter_scale/models/product_model.dart';
import 'package:flutter_scale/screens/tabbarmenu/widgets/product_item_list.dart';
import 'package:flutter_scale/services/rest_api.dart';
import 'package:flutter_scale/themes/colors.dart';

var refreshStateKey = GlobalKey<RefreshIndicatorState>();

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({Key? key}) : super(key: key);

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  void refreshState() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
          key: refreshStateKey,
          child: FutureBuilder(
            future: CallApi().getProducts(),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return const Center(
                  child: Text('เกิดข้อผิดพลาดในการแสดงข้อมูล'),
                );
              } else if (snapshot.connectionState == ConnectionState.done) {
                // * แสดงข้อมูล
                List<ProductModel> products = snapshot.data!;
                return listViewProduct(products, refreshState);
              } else {
                // * แสดง Loading กรณียังโหลดข้อมูลไม่สำเร็จ
                return const Center(
                  child: CircularProgressIndicator(color: primaryLight),
                );
              }
            },
          ),
          onRefresh: () async {
            setState(() {});
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/addproduct');
        },
        backgroundColor: accent,
        splashColor: primaryDark,
        child: const Icon(
          Icons.add,
          color: textIcons,
          size: 30,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
