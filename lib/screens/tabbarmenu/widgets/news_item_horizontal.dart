import 'package:flutter/material.dart';
import 'package:flutter_scale/models/news_model.dart';
import 'package:flutter_scale/themes/colors.dart';

Widget lastNewsItemList(List<NewsModel> news) {
  return ListView.builder(
    scrollDirection: Axis.horizontal,
    itemCount: news.length,
    itemBuilder: (context, index) {
      NewsModel newsModel = news[index];
      return Container(
        width: (MediaQuery.of(context).size.width * 0.6),
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(context, '/newsdetail',
                arguments: {'id': newsModel.id});
          },
          child: Card(
            child: Container(
              child: Column(
                children: [
                  Container(
                    height: 125,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            alignment: Alignment.topCenter,
                            image: NetworkImage(newsModel.imageurl))),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(children: [
                      Text(
                        newsModel.topic,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        newsModel.detail,
                        overflow: TextOverflow.ellipsis,
                        style:
                            const TextStyle(fontSize: 14, color: secondaryText),
                      ),
                    ]),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    },
  );
}
