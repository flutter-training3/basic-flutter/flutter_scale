import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scale/models/news_model.dart';

Widget newsItemList(List<NewsModel> news) {
  return ListView.builder(
    itemCount: news.length,
    itemBuilder: (context, index) {
      NewsModel newsModel = news[index];
      return ListTile(
        onTap: () {
          Navigator.pushNamed(context, '/newsdetail',
              arguments: {'id': newsModel.id});
        },
        contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        leading: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Image.network(newsModel.imageurl),
        ),
        title: Text(
          newsModel.topic,
          overflow: TextOverflow.ellipsis,
        ),
        subtitle:
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            newsModel.detail,
            overflow: TextOverflow.ellipsis,
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            formatDate(newsModel.createdAt!,
                [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn]),
            style: const TextStyle(fontSize: 12),
          )
        ]),
      );
    },
  );
}
