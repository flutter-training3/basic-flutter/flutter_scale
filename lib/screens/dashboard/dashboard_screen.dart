import 'package:flutter/material.dart';
import 'package:flutter_scale/screens/buttonnavmenu/home_screen.dart';
import 'package:flutter_scale/screens/buttonnavmenu/notification_screen.dart';
import 'package:flutter_scale/screens/buttonnavmenu/profile_screen.dart';
import 'package:flutter_scale/screens/buttonnavmenu/report_screen.dart';
import 'package:flutter_scale/screens/buttonnavmenu/setting_screen.dart';
import 'package:flutter_scale/themes/colors.dart';
import 'package:flutter_scale/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  // * สร้าง Object SharedPreferences
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  // * สร้าง parameter ที่เก็บข้อมูลจาก Shared Preferences
  String? _fullname, _username, _avatar, _status;

  // * สร้างตัวแปรไว้เก็บลำดับของ List
  int _currentIndex = 0;
  // * สร้างตัวแปรสำหรับเก็บ title
  String _title = 'หน้าแรก';
  // * สร้างตัวแปร List เก็บหน้า Screen ของ BottomNavigation
  final List<Widget> _children = [
    const HomeScreen(),
    const ReportScreen(),
    const NotificationScreen(),
    const SettingScreen(),
    const ProfileScreen()
  ];

  // * สร้าง function สำหรับการเปลี่ยน tap
  void _onTapChange(int index) {
    setState(() {
      _currentIndex = index;
      switch (index) {
        case 0:
          _title = 'หน้าหลัก';
          break;
        case 1:
          _title = 'รายงาน';
          break;
        case 2:
          _title = 'แจ้งเตือน';
          break;
        case 3:
          _title = 'ตั้งค่า';
          break;
        case 4:
          _title = 'โปรไฟล์';
          break;
      }
    });
  }

  // * สร้างฟังก์ชั่นสำหรับอ่านข้่อมูลจาก Profile จาก Shared Preferences
  readUserProfile() async {
    final SharedPreferences prefs = await _prefs;
    setState(() {});
    _username = prefs.getString('username');
    _fullname = prefs.getString('fullname');
    _avatar = prefs.getString('img_profile');
    _status = prefs.getString('status');
  }

  // * สร้าง function logout
  _onLogOut() async {
    final SharedPreferences prefs = await _prefs;
    // * Clear Shared Preferences
    prefs.clear();
    Navigator.pushReplacementNamed(context, '/login');
  }

  // * เรียกใช้งาน  method initState
  @override
  void initState() {
    super.initState();
    readUserProfile();
  }

  // * สร้างฟังก์ชั่นสำหรับ check การกดปุ่ม back for android
  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('Are you sure ?'),
              content: const Text('Do you want to exit an app'),
              actions: [
                TextButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: const Text('No')),
                TextButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: const Text('Yes')),
              ],
            );
          },
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          title: Text(_title),
        ),
        body: _children[_currentIndex],
        // * Drawer Menu
        drawer: Drawer(
          backgroundColor: primary,
          child: ListView(
            children: [
              UserAccountsDrawerHeader(
                currentAccountPicture: _avatar != null
                    ? CircleAvatar(
                        radius: 60,
                        backgroundColor: primaryDark,
                        backgroundImage:
                            NetworkImage('${baseImageUrl}profile/${_avatar!}'))
                    : const CircularProgressIndicator(),
                accountName: Text(
                  _fullname ?? '...',
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                accountEmail: Text(_username ?? '...'),
                decoration: const BoxDecoration(color: primaryDark),
              ),
              ListTile(
                leading: const Icon(
                  Icons.person,
                  color: textIcons,
                ),
                title: const Text(
                  'เกี่ยวกับเรา',
                  style:
                      TextStyle(color: textIcons, fontWeight: FontWeight.bold),
                ),
                onTap: () => Navigator.pushNamed(context, '/about'),
              ),
              ListTile(
                leading: const Icon(
                  Icons.info,
                  color: textIcons,
                ),
                title: const Text(
                  'ข้อมูลการใช้งาน',
                  style:
                      TextStyle(color: textIcons, fontWeight: FontWeight.bold),
                ),
                onTap: () => Navigator.pushNamed(context, '/info'),
              ),
              ListTile(
                leading: const Icon(
                  Icons.contact_mail,
                  color: textIcons,
                ),
                title: const Text(
                  'ติดต่อเรา',
                  style:
                      TextStyle(color: textIcons, fontWeight: FontWeight.bold),
                ),
                onTap: () => Navigator.pushNamed(context, '/contact'),
              ),
              ListTile(
                leading: const Icon(
                  Icons.exit_to_app,
                  color: textIcons,
                ),
                title: const Text(
                  'ออกจากระบบ',
                  style:
                      TextStyle(color: textIcons, fontWeight: FontWeight.bold),
                ),
                onTap: _onLogOut,
              )
            ],
          ),
        ),
        // * Bottom Navigation Bar
        bottomNavigationBar: BottomNavigationBar(
            onTap: _onTapChange,
            currentIndex: _currentIndex,
            type: BottomNavigationBarType.fixed,
            iconSize: 22,
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: 'หน้าแรก'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.show_chart), label: 'รายงาน'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.notification_important_rounded),
                  label: 'แจ้งเตือน'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.settings), label: 'ตั้งค่า'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person), label: 'โปรไฟล์'),
            ]),
      ),
    );
  }
}
