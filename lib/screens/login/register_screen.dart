// ignore_for_file: use_build_context_synchronously

import 'dart:convert';
import 'dart:ffi';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scale/components/custom_widgets.dart';
import 'package:flutter_scale/services/rest_api.dart';
import 'package:flutter_scale/themes/colors.dart';
import 'package:flutter_scale/utils/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  // * สร้าง Object SharedPreferences
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  // * สร้าง Key ไว้ผูกกับ form
  final formKey = GlobalKey<FormState>();
  // * สร้างตัวแปรไว้รับค่าจาก form
  late String _username, _password, _fullname;

  // * สร้างฟังก์ชั่น Validate Input
  _validateFunction(String validate, String errorText) {
    if (validate.isEmpty) {
      return errorText;
    } else {
      return null;
    }
  }

  // * สร้างฟังก์ชั่น submitRegister
  void _submitRegister() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      var utility = Utility.getInstance();
      String checkNetwork = await utility!.checkNetwork();
      if (checkNetwork == '') {
        utility.showAlertDialog(context,
            altTitle: 'มีข้อผิดพลาดเกิดขึ้น !',
            altContent: 'อุปกรณ์ของคุณไม่ได้เชื่อมต่ออินเตอร์เน็ต!',
            altBtnText: 'ตกลง');
      } else {
        var response = await CallApi().registerApi({
          "username": _username,
          "password": _password,
          "fullname": _fullname,
          "status": 1
        });
        var body = json.decode(response.body);
        if (body['status'] == 'success') {
          Navigator.pushReplacementNamed(context, '/login');
        } else {
          utility.showAlertDialog(context,
              altTitle: 'มีข้อผิดพลาดเกิดขึ้น !',
              altContent:
                  'ไม่สามารถสมัครสมาชิกได้ กรุณาตรวจสอบข้อมูลใหม่อีกครั้ง!',
              altBtnText: 'ตกลง');
        }
      }
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Login'),
      // ),
      body: SingleChildScrollView(
          child: Form(
              key: formKey,
              child: SizedBox(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 50,
                    ),
                    Image.asset(
                      'assets/images/welcome-icon.png',
                      width: (MediaQuery.of(context).size.width * 0.3),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      'สมัครสมาชิก',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 22,
                          color: primaryDark),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: inputFieldWidget(
                            context,
                            const Icon(Icons.person),
                            'ชื่อ - นามสกุล',
                            'กรุณากรอกข้อมูลชื่อ - นามสกุล',
                            _validateFunction, (valueSave) {
                          _fullname = valueSave;
                        }, maxLenght: 100),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: inputFieldWidget(
                            context,
                            const Icon(Icons.login),
                            'ชื่อผู้ใช้งาน',
                            'กรุณากรอกข้อมูลชื่อผู้ใช้งาน',
                            _validateFunction, (valueSave) {
                          _username = valueSave;
                        }),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: inputFieldWidget(
                            context,
                            const Icon(Icons.lock_outline),
                            'รหัสผู้ใช้งานระบบ',
                            'กรุณากรอกข้อมูลรหัสผู้ใช้งานระบบ',
                            _validateFunction, (valueSave) {
                          _password = valueSave;
                        }, obsecureText: true),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: submitButton('สมัครสมาชิก', primaryDark,
                          primaryLight, _submitRegister),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text('คุณเป็นสมาชิกอยู่แล้ว ?'),
                    TextButton(
                        onPressed: () {
                          Navigator.pushReplacementNamed(context, '/login');
                        },
                        child: const Text(
                          'เข้าสู่ระบบ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ))
                  ],
                ),
              ))),
    );
  }
}
