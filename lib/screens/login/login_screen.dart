import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scale/services/rest_api.dart';
import 'package:flutter_scale/themes/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // * สร้าง Object SharedPreferences
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  // * สร้าง Key ไว้ผูกกับ form
  final formKey = GlobalKey<FormState>();
  // * สร้างตัวแปรไว้รับค่าจาก form
  late String _username, _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Login'),
      // ),
      body: SingleChildScrollView(
          child: Form(
              key: formKey,
              child: SizedBox(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 100,
                    ),
                    Image.asset(
                      'assets/images/welcome-icon.png',
                      width: (MediaQuery.of(context).size.width * 0.5),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      'เข้าสู่ระบบ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 22,
                          color: primaryDark),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: TextFormField(
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'กรุณากรอกข้อมูลผู้ใช้งาน';
                            } else {
                              return null;
                            }
                          },
                          onSaved: (newValue) {
                            _username = newValue.toString().trim();
                          },
                          decoration:
                              const InputDecoration(labelText: 'ชื่อผู้ใช้งาน'),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: TextFormField(
                          obscureText: true,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'กรุณากรอกข้อมูลรหัสผ่าน';
                            } else {
                              return null;
                            }
                          },
                          onSaved: (newValue) {
                            _password = newValue.toString().trim();
                          },
                          decoration:
                              const InputDecoration(labelText: 'รหัสผ่าน'),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: ElevatedButton(
                            onPressed: () async {
                              // * เช็คว่าป้อนค่าใน form ผ่านหรือยัง
                              if (formKey.currentState!.validate()) {
                                formKey.currentState!.save();

                                // Check Status Network
                                var checkNetwork =
                                    await Connectivity().checkConnectivity();
                                if (checkNetwork == ConnectivityResult.none) {
                                  // ! Show Alert Dialog
                                  AlertDialog alert = AlertDialog(
                                    title: const Text('มีข้อผิดพลาดเกิดขึ้น !'),
                                    content: const Text(
                                        'กรุณาตรวจสอบอินเตอร์เน็ตของคุณอีกครั้ง'),
                                    actions: [
                                      TextButton(
                                          onPressed: () =>
                                              Navigator.pop(context),
                                          child: const Text('ตกลง'))
                                    ],
                                  );
                                  showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) => alert,
                                  );
                                } else {
                                  // Call Login API
                                  var response = await CallApi().loginApi({
                                    "username": _username,
                                    "password": _password
                                  });
                                  var body = json.decode(response.body);
                                  if (body['status'] == 'success') {
                                    // * สร้าง Object แบบ SharedPreference
                                    final SharedPreferences prefs =
                                        await _prefs;
                                    prefs.setString(
                                        "userId", body['data']['id']);
                                    prefs.setInt('userStep', 1);
                                    prefs.setString(
                                        "username", body['data']['username']);
                                    prefs.setString(
                                        'fullname', body['data']['fullname']);
                                    prefs.setString('img_profile',
                                        body['data']['img_profile']);
                                    prefs.setString(
                                        'status', body['data']['status']);
                                    // * Login Success
                                    // * Navigator ไป หน้าแรก
                                    Navigator.of(context)
                                        .pushReplacementNamed('/dashboard');
                                  } else {
                                    // ! Show Alert Dialog
                                    AlertDialog alert = AlertDialog(
                                      title:
                                          const Text('มีข้อผิดพลาดเกิดขึ้น !'),
                                      content: const Text(
                                          'ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบชื่อผู้ใช้งาน หรือรหัสผ่าน'),
                                      actions: [
                                        TextButton(
                                            onPressed: () =>
                                                Navigator.pop(context),
                                            child: const Text('ตกลง'))
                                      ],
                                    );
                                    showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (context) => alert,
                                    );
                                  }
                                }
                              } else {
                                // ! Show Alert Dialog
                                AlertDialog alert = AlertDialog(
                                  title: const Text('มีข้อผิดพลาดเกิดขึ้น !'),
                                  content: const Text(
                                      'กรุณาตรวจสอบข้อมูลของคุณใหม่อีกครั้ง'),
                                  actions: [
                                    TextButton(
                                        onPressed: () => Navigator.pop(context),
                                        child: const Text('ตกลง'))
                                  ],
                                );
                                showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (context) => alert,
                                );
                              }
                            },
                            child: const Text('เข้าสู่ระบบ')),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text('ยังไม่เป็นสมาชิก ?'),
                    TextButton(
                        onPressed: () {
                          Navigator.pushReplacementNamed(context, '/register');
                        },
                        child: const Text(
                          'สมัครสมาชิก',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ))
                  ],
                ),
              ))),
    );
  }
}
