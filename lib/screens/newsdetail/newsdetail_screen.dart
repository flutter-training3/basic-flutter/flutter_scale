import 'package:flutter/material.dart';
import 'package:flutter_scale/models/news_detail_model.dart';
import 'package:flutter_scale/services/rest_api.dart';
import 'package:flutter_scale/themes/colors.dart';

class NewsdetailScreen extends StatefulWidget {
  const NewsdetailScreen({Key? key}) : super(key: key);

  @override
  State<NewsdetailScreen> createState() => _NewsdetailScreenState();
}

class _NewsdetailScreenState extends State<NewsdetailScreen> {
  // เรียกใช้งาน NewDetailModel
  NewsDetailModel? _dataNews;
  bool isCall = false;

  // สร้างฟังก์ชั่นอ่านรายละเอียดข่าวตาม id
  readNewsDetail(id) async {
    try {
      if (!isCall) {
        var response = await CallApi().getNewsDetail(id);
        setState(() {});
        _dataNews = response!;
        isCall = true;
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    // รับค่า arguments
    final Map arguments = ModalRoute.of(context)!.settings.arguments as Map;
    print(arguments['id']);
    readNewsDetail(arguments['id']);
    return Scaffold(
      appBar: AppBar(
        title: Text(_dataNews?.topic ?? ''),
      ),
      body: _dataNews != null
          ? ListView(
              children: [
                Container(
                  height: 200,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(_dataNews?.imageurl ?? '...'),
                          fit: BoxFit.cover)),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: Text(
                    _dataNews?.topic ?? '...',
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: Text(
                    _dataNews?.detail ?? '...',
                    style: const TextStyle(fontSize: 16),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: Text(
                    'Publish Date : ${_dataNews?.createdAt.toString()}' ??
                        '...',
                    style: const TextStyle(fontSize: 14),
                  ),
                )
              ],
            )
          : const Center(
              child: CircularProgressIndicator(
                color: primaryDark,
              ),
            ),
    );
  }
}
