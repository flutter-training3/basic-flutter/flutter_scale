import 'package:flutter/material.dart';
import 'package:flutter_scale/screens/tabbarmenu/news_screen.dart';
import 'package:flutter_scale/screens/tabbarmenu/products_screen.dart';
import 'package:flutter_scale/screens/tabbarmenu/stroes_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: PreferredSize(
              preferredSize: const Size.fromHeight(50),
              child: AppBar(
                bottom: const TabBar(tabs: [
                  Tab(
                    child: Text('ข่าวสาร'),
                  ),
                  Tab(
                    child: Text('สินค้า'),
                  ),
                  Tab(
                    child: Text('สโตร์'),
                  )
                ]),
              )),
          body: const TabBarView(
              children: [NewsScreen(), ProductsScreen(), StroesScreen()]),
        ));
  }
}
