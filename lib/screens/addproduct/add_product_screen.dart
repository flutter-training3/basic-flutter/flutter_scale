// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_scale/components/custom_widgets.dart';
import 'package:flutter_scale/models/product_model.dart';
import 'package:flutter_scale/services/rest_api.dart';
import 'package:flutter_scale/themes/colors.dart';
import 'package:flutter_scale/utils/utility.dart';

class AddProductScreen extends StatefulWidget {
  const AddProductScreen({Key? key}) : super(key: key);

  @override
  State<AddProductScreen> createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  final formKey = GlobalKey<FormState>();
  String? _productName,
      _productDetail,
      _productBarcode,
      _productPrice,
      _productQty,
      _productImage;

  // * สร้างฟังก์ชั่น Validate Input
  _validateFunction(String validate, String errorText) {
    if (validate.isEmpty) {
      return errorText;
    } else {
      return null;
    }
  }

  void _submitProduct() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      var utility = Utility.getInstance();
      String checkNetwork = await utility!.checkNetwork();
      if (checkNetwork == '') {
        utility.showAlertDialog(context,
            altTitle: 'มีข้อผิดพลาดเกิดขึ้น !',
            altContent: 'อุปกรณ์ของคุณไม่ได้เชื่อมต่ออินเตอร์เน็ต!',
            altBtnText: 'ตกลง');
      } else {
        ProductModel productModel = ProductModel(
            productDetail: _productDetail!,
            productName: _productName!,
            productBarcode: _productBarcode!,
            productQty: _productQty!,
            productPrice: _productPrice!,
            productImage: _productImage!);
        var response = await CallApi().createProduct(productModel);
        if (response) {
          Navigator.pop(context);
        } else {
          utility.showAlertDialog(context,
              altTitle: 'มีข้อผิดพลาดเกิดขึ้น !',
              altContent:
                  'ไม่สามารถบันทึกข้อมูลสินค้า กรุณาตรวจสอบข้อมูลใหม่อีกครั้ง!',
              altBtnText: 'ตกลง');
        }
      }
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('เพิ่มสินค้าใหม่'),
      ),
      body: Form(
          key: formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 15),
            child: ListView(
              children: [
                inputFieldWidget(
                    context,
                    const Icon(Icons.production_quantity_limits_outlined),
                    'ชื่อสินค้า',
                    'กรุณากรอกข้อมูลชื่อสินค้า',
                    _validateFunction, (valueSave) {
                  _productName = valueSave;
                }, maxLenght: 100),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(
                    context,
                    const Icon(Icons.list_alt_outlined),
                    'รายละเอียดสินค้า',
                    'กรุณากรอกข้อมูลรายละเอียดสินค้า',
                    _validateFunction, (valueSave) {
                  _productDetail = valueSave;
                },
                    maxLenght: 500,
                    keyboardType: TextInputType.multiline,
                    maxLine: 5),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(context, const Icon(Icons.code), 'บาร์โค๊ด',
                    'กรุณากรอกข้อมูลบาร์โค๊ด', _validateFunction, (valueSave) {
                  _productBarcode = valueSave;
                }, maxLenght: 50, keyboardType: TextInputType.number),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(
                    context,
                    const Icon(Icons.price_change),
                    'ราคาสินค้า',
                    'กรุณากรอกข้อมูลราคาสินค้า',
                    _validateFunction, (valueSave) {
                  _productPrice = valueSave;
                },
                    maxLenght: 10,
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true)),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(
                    context,
                    const Icon(Icons.store),
                    'จำนวนสินค้า',
                    'กรุณากรอกข้อมูลจำนวนสินค้า',
                    _validateFunction, (valueSave) {
                  _productQty = valueSave;
                }, maxLenght: 10, keyboardType: TextInputType.number),
                const SizedBox(
                  height: 15,
                ),
                inputFieldWidget(
                    context,
                    const Icon(Icons.image),
                    'รูปภาพสินค้า',
                    'กรุณากรอกข้อมูลรูปภาพสินค้า',
                    _validateFunction, (valueSave) {
                  _productImage = valueSave;
                }, maxLenght: 100, keyboardType: TextInputType.url),
                const SizedBox(
                  height: 15,
                ),
                submitButton(
                    'บันทึกข้อมูล', primaryDark, primaryLight, _submitProduct),
              ],
            ),
          )),
    );
  }
}
