import 'package:flutter/material.dart';
import 'package:flutter_scale/routers.dart';
import 'package:flutter_scale/themes/style.dart';
import 'package:shared_preferences/shared_preferences.dart';

var userStep;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // * สร้าง Object SharedPreferences
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var prefs = await _prefs;
  userStep = prefs.getInt('userStep');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: appTheme(),
      initialRoute: userStep == 1 ? '/dashboard' : '/welcome',
      routes: routes,
    );
  }
}
